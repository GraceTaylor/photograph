# Photograph

If you are looking for ways to generate a preview or a thumbnail of your web-based document, Photograph is the solution for you. This tool will provide you the preview of the exact content of your Google Spreadsheet or any documents online. You don't have to worry whether you can read a data or not. Photograph also supports cropping to avoid reworking afterward.

Check out more of Photograph [here](https://www.zipngoinsurance.com). 

## Installation

Add this line to your application's Gemfile:

    gem 'photograph'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install photograph

## Usage

Photograph can be used either directly through the Photograph::Artist
class or by its little sinata app. 

    @artist = Photograph::Artist.new("http://github.com")
    @artist.shoot!

    @artist.image
    # => MiniMagick instance you can toy with

Or 

    $ bundle exec photograph -h 127.0.0.1 -p 4567

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Credits


Photograph is maintained and funded by Tactilize.

Contributors : 

* Jean-Hadrien Chabran

The names and logos for Tactilize are trademarks of Tactilize.

## License

Photograph is Copyright © 2012 Tactilize. It is free software, and may be redistributed under the terms specified in the MIT-LICENSE file.